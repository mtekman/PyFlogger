### Python Food Logger

Generates calorific stats of all foods logged based on calories, protein, fat, fibre, and sugar -- represented in a CLI pie chart.
